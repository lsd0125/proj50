<?php
require __DIR__. '/__connect_db.php';

$page_name = 'product_list';

$cate = isset($_GET['cate']) ? intval($_GET['cate']) : 0;

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$page = $page <= 0 ? 1 : $page;
// $page <= 0 ? $page=1 : '';

$per_page = 4;

$w = '';
$param = '';

if(! empty($cate)){
    $w = " WHERE `category_sid`=$cate ";
    $param = "&cate=$cate";
}
$t_sql = "SELECT COUNT(1) FROM `products` $w";

$t_rs = $mysqli->query($t_sql);
$total_rows = $t_rs->fetch_row()[0];

$pages = ceil($total_rows/$per_page);



$sql = sprintf("SELECT * FROM `products` $w ORDER BY `sid` ASC  LIMIT %s, %s",
    ($page-1)*$per_page,
    $per_page
);



//$sql = "SELECT * FROM `products`";
$rs = $mysqli->query($sql);


// --- 分類

$c_sql = "SELECT * FROM `categories` ORDER BY `sid`";

$c_rs = $mysqli->query($c_sql);




?>
<?php include __DIR__. '/__html_head.php'; ?>
<style>
    .product-img {
        width: 100px;
        height:135px;
        margin-left: auto;
        margin-right: auto;
    }

</style>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <div class="row" style="margin-top: 30px">
        <div class="col-md-3">
            <div class="btn-group-vertical" style="width: 100%">
                <button type="button" class="cate btn <?= empty($cate) ? 'btn-primary disabled' : 'btn-outline-primary' ?>" data-cate="0">全部</button>
                <?php while($c_row = $c_rs->fetch_assoc()):
                    if($c_row['parent_sid']==0):
                    ?>
                <button type="button" class="cate btn <?= $cate==$c_row['sid'] ? 'btn-primary disabled' : 'btn-outline-primary' ?>" data-cate="<?= $c_row['sid'] ?>">
                    <?= $c_row['name'] ?>
                </button>
                <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </div>


        <div class="col-md-9">
            <div class="row">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item <?= $page<=1 ? 'disabled' : '' ?>">
                            <a class="page-link" href="?page=<?= $page<=1 ? 1 : $page-1 ?><?= $param ?>" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <?php for($i=1; $i<=$pages; $i++): ?>
                        <li class="page-item <?= $page==$i ? 'active' : '' ?>">
                            <a class="page-link" href="?page=<?= $i. $param ?>"><?= $i ?></a>
                        </li>
                        <?php endfor; ?>
                        <li class="page-item <?= $page<$pages ? '' : 'disabled' ?>">
                            <a class="page-link" href="?page=<?= $page<$pages ? $page+1 : $pages ?><?= $param ?>" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row">
                <?php for($i=0; $i<4; $i++):
                    $row = $rs->fetch_assoc();
                ?>
                <div class="col">
                    <?php if(! empty($row)): ?>
                    <div class="card" style="">
                        <img class="product-img" src="./imgs/small/<?= $row['book_id'] ?>.jpg" >
                        <div class="card-body">
                            <h6 class="card-title" style="word-break: break-all"><?= $row['bookname'] ?></h6>
                            <p class="card-text"><?= $row['author'] ?></p>
                            <p class="card-text">價格: <?= $row['price'] ?></p>
                            <select class="custom-select mb-2 mr-sm-2 mb-sm-0">
                                <?php for($k=1; $k<=10; $k++): ?>
                                <option value="<?= $k ?>"><?= $k ?></option>
                                <?php endfor; ?>

                            </select>
                            <button class="btn btn-primary buy-btn" data-sid="<?= $row['sid'] ?>">
                                <i class="fa fa-cart-arrow-down"></i>
                            </button>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <?php endfor ?>
            </div>

        </div>

    </div>



</div>
<script>
    $('.cate.btn').click(function(){
        var cate = $(this).attr('data-cate');

        location.href = '?cate=' + cate;
    });

    $('button.buy-btn').click(function(){
        var sid = $(this).attr('data-sid');

        var qty = $(this).closest('.card').find('select').val();

        $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
            console.log(data);
            showCartCount(data);
            alert('感謝加入購物車');
        }, 'json');

    });



</script>
<?php include __DIR__. '/__html_foot.php'; ?>


<?php
require __DIR__. '/__connect_db.php';

if(!isset($_SESSION['user'])){
    echo json_encode(array(
        'success' => false,
        'error' => '沒有登入',
    ));
    exit;
}

$result = array(
    'success' => false,
    'error' => '沒有給足夠的參數',
    'post' => $_POST,
);

if(!empty($_POST['password']) and !empty($_POST['nickname'])) {
    $pass = sha1($_POST['password']); //破壞性加密

    $sql = "UPDATE `members` SET 
                `mobile`=?,
                `address`=?,
                `birthday`=?,
                `nickname`=?
                 WHERE `id`=? AND `password`=?";

    $stmt = $mysqli->prepare($sql);
    //echo $mysqli->error; //除錯

    $stmt->bind_param('ssssis',
        $_POST['mobile'],
        $_POST['address'],
        $_POST['birthday'],
        $_POST['nickname'],
        $_SESSION['user']['id'],
        $pass
        );

    $stmt->execute();

    if($stmt->affected_rows==1) {
        $_SESSION['user']['mobile'] = $_POST['mobile'];
        $_SESSION['user']['address'] = $_POST['address'];
        $_SESSION['user']['birthday'] = $_POST['birthday'];
        $_SESSION['user']['nickname'] = $_POST['nickname'];

        $result['success'] = true;

    } else {
        $result['error'] = '密碼錯誤或資料未變更';
    }
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);



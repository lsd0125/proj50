<?php
require __DIR__ . '/__connect_db.php';

$page_name = 'cart_list';

if(! empty($_SESSION['cart'])){
    $keys = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM `products` WHERE sid IN (%s)", implode(',', $keys));

    $rs = $mysqli->query($sql);

    $cart_data = array();

    while($r = $rs->fetch_assoc()) {

        $r['qty'] = $_SESSION['cart'][$r['sid']];

        $cart_data[ $r['sid'] ] = $r;

    }

    //$rs->data_seek(0); //recordset 內部指標指到第一筆

}




?>
<?php include __DIR__ . '/__html_head.php'; ?>
<style>
    .remove-btn {
        font-size: x-large;
        color: red;
    }
</style>
<div class="container">
    <?php include __DIR__ . '/__navbar.php'; ?>


    <div class="row" style="margin-top: 30px">
        <div class="col">
            <?php if(isset($cart_data)): ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>移除</th>
                    <th>封面</th>
                    <th>書名</th>
                    <th>價格</th>
                    <th>數量</th>
                    <th>小計</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($keys as $k):
                $row = $cart_data[$k];
                ?>
                <tr id="item<?= $row['sid'] ?>" class="data-row" data-sid="<?= $row['sid'] ?>">
                    <td>
                        <a href="javascript:removeItem(<?= $row['sid'] ?>)">
                            <i class="fa fa-remove remove-btn" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td><img src="./imgs/small/<?= $row['book_id'] ?>.jpg" alt=""></td>
                    <td><?= $row['bookname'] ?></td>
                    <td class="price"><?= $row['price'] ?></td>
                    <td>
                        <select class="custom-select mb-2 mr-sm-2 mb-sm-0">
                            <?php for($k=1; $k<=20; $k++): ?>
                                <option value="<?= $k ?>"
                                  <?= $row['qty']==$k ? 'selected' : '' ?>
                                >
                                    <?= $k ?></option>
                            <?php endfor; ?>
                        </select>
                    </td>
                    <td class="sub-total"><?= $row['qty']*$row['price']  ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
                <div class="alert alert-danger" role="alert">
                    購物車內沒有商品
                </div>
                <a class="btn btn-primary pull-right" href="product_list.php">到 商品列表</a>
            <?php endif; ?>
        </div>

    </div>

    <?php if(!empty($_SESSION['cart'])) : ?>
    <div class="row">
        <div class="col">
            <div class="alert alert-success" role="alert">
                總計: <span id="total_amount"></span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?php if(isset($_SESSION['user'])): ?>
                <a class="btn btn-primary pull-right" href="buy.php">進入結帳</a>
            <?php else: ?>
                <a class="btn btn-danger pull-right" href="login.php">請先登入再結帳</a>
            <?php endif; ?>
        </div>
    </div>
    <?php endif; ?>
</div>
<script>
    function removeItem(sid){

        $.get('add_to_cart.php', {sid:sid}, function(data){
            showCartCount(data);
            $('#item'+sid).remove();
            calTotalAmount();

        }, 'json');
    }


    function calTotalAmount(){
        var total = 0;
        $('.data-row').each(function(){
            var sub = $(this).find('.sub-total').text();
            total += parseInt(sub);

        });

        $('#total_amount').text(total);
    }

    calTotalAmount();

    $('.data-row select').change(function(){
        var me = $(this);
        var tr = me.closest('.data-row');
        var qty = me.val();
        var sid = tr.attr('data-sid');

        $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
            showCartCount(data);
            var price = tr.find('.price').text();
            tr.find('.sub-total').text(price * qty);

            calTotalAmount();
        }, 'json');
    });



</script>
<?php include __DIR__ . '/__html_foot.php'; ?>


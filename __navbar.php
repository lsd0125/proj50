    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">小新的店</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?= $page_name=='product_list' ? 'active' : '' ?>">
                    <a class="nav-link" href="product_list.php">產品列表</a>
                </li>

                <li class="nav-item <?= $page_name=='cart_list' ? 'active' : '' ?>">
                    <a class="nav-link" href="cart_list.php">購物車
                        <span class="badge badge-primary cart-count" style="display: none">0</span>
                    </a>
                </li>

            </ul>
            <?php if(isset($_SESSION['user'])): ?>
                <ul class="navbar-nav">
                    <li class="nav-item ">
                        <a class="nav-link" id="nickname"><?= $_SESSION['user']['nickname'] ?></a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="edit_user.php">編輯會員資料</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="history.php">歷史訂單</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="logout.php">登出</a>
                    </li>

                </ul>
            <?php else: ?>
            <ul class="navbar-nav">
                <li class="nav-item <?= $page_name=='login' ? 'active' : '' ?>">
                    <a class="nav-link" href="login.php">登入</a>
                </li>
                <li class="nav-item <?= $page_name=='register' ? 'active' : '' ?>">
                    <a class="nav-link" href="register.php">註冊</a>
                </li>

            </ul>
            <?php endif; ?>
        </div>
    </nav>

    <script>
        var cart_count = $('.cart-count');


        $.get('add_to_cart.php', function(data){
            //console.log(data);

            showCartCount(data);
            cart_count.show();
        }, 'json');


        function showCartCount(obj){
            var s, count=0;
            for(s in obj){
                count += obj[s];
            }
            $('.cart-count').text(count);
        }
    </script>
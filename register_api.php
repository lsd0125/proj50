<?php
require __DIR__. '/__connect_db.php';

$result = array(
        'success' => false,
        'error' => '沒有給足夠的參數',
);

if(!empty($_POST['email']) and !empty($_POST['password']) and !empty($_POST['nickname'])) {

    $pass = sha1($_POST['password']); //破壞性加密
    $hash = md5($_POST['email']. uniqid() );

    $sql = "INSERT INTO `members`(
            `email`, `password`, `mobile`,
            `address`, `birthday`, `hash`,
            `activated`, `nickname`, `create_at`
            ) VALUES (
            ?, ?, ?,
            ?, ?, ?,
            0, ?, NOW())
            ";

    $stmt = $mysqli->prepare($sql);
    //echo $mysqli->error; //除錯

    $stmt->bind_param('sssssss',
        $_POST['email'],
        $pass,
        $_POST['mobile'],

        $_POST['address'],
        $_POST['birthday'],
        $hash,
        $_POST['nickname']
        );

    $stmt->execute();

    if($stmt->affected_rows==1) {
        $result = array(
            'success' => true,
            'error' => '',
        );
    } elseif ($stmt->affected_rows==-1) {
        $result['error'] = '此 email 已經被使用過了';
    } else {
        $result['error'] = '發生錯誤';
    }
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);



<?php require __DIR__. '/__connect_db.php';

$page_name = 'register';


?>
<?php include __DIR__. '/__html_head.php'; ?>
<style>
    .form-group label span {
        color:red;
    }
    small.form-text {
        display: none;
    }
    #my-alert {
        display: none;
    }
</style>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <div id="my-alert" class="alert alert-success" role="alert">
    </div>


    <div class="row justify-content-md-center" style="margin-top: 30px">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    註冊會員
                </div>
                <div class="card-body">
                    <!-- `name`, `mobile`, `email`, `birthday`, `address`  -->
                    <form name="form1" method="post" onsubmit="return checkForm()">
                        <div class="form-group">
                            <label for="email"><span class="hint">*</span> 電郵</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="">
                            <small id="emailHelp" class="form-text">請填入正確電子郵箱格式</small>
                        </div>
                        <div class="form-group">
                            <label for="password"><span class="hint">*</span> 密碼</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="">
                            <small id="nameHelp" class="form-text">請填密碼</small>
                        </div>
                        <div class="form-group">
                            <label for="nickname"><span class="hint">*</span> 暱稱</label>
                            <input type="text" class="form-control" id="nickname" name="nickname" placeholder="">
                            <small id="nicknameHelp" class="form-text">請填暱稱</small>
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="">
                            <small id="mobileHelp" class="form-text">請填十碼手機號碼</small>
                        </div>

                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" placeholder="">
                            <small id="birthdayHelp" class="form-text">請填生日</small>
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <textarea  class="form-control" name="address" id="address" cols="50" rows="3" placeholder="地址"></textarea>
                            <small id="addressHelp" class="form-text">請填地址</small>
                        </div>


                        <button type="submit" class="btn btn-primary">新增</button>
                    </form>


                </div>
            </div>

        </div>
    </div>

</div>
<script>
    var my_alert = $('#my-alert');

    var btn = $('form button');

    function checkForm(){
        var isPass = true;

        if(! document.form1.email.value){
            isPass = false;
            alert('email 沒填');
        }
        if(! document.form1.password.value){
            isPass = false;
            alert('密碼沒填');
        }
        if(! document.form1.nickname.value){
            isPass = false;
            alert('暱稱沒填');
        }

        if(isPass){
            btn.hide();
            my_alert.hide();

            $.post('register_api.php', $(document.form1).serialize(), function(data){
                console.log(data);
                my_alert.show();
                if(data.success){
                    my_alert.attr('class', 'alert alert-success');
                    my_alert.html("新增完成");

                } else {
                    my_alert.attr('class', 'alert alert-danger');
                    btn.show();
                    my_alert.html(data.error);
                }
            }, 'json');

        }

        return false;
    }


</script>
<?php include __DIR__. '/__html_foot.php'; ?>


<?php
require __DIR__. '/__connect_db.php';

if(empty($_SESSION['cart']) or !isset($_SESSION['user'])){
    header("Location: login.php");
    exit;
}

// 取得購物車內的商品資料, 同 cart_list 前面
$total = 0; //總價

$keys = array_keys($_SESSION['cart']);
$sql = sprintf("SELECT * FROM `products` WHERE sid IN (%s)", implode(',', $keys));
$rs = $mysqli->query($sql);
$cart_data = array();
while($r = $rs->fetch_assoc()) {
    $r['qty'] = $_SESSION['cart'][$r['sid']];
    $cart_data[ $r['sid'] ] = $r;

    $total += $r['qty']*$r['price'];
}


// 1. 寫入 orders 表
$sql = sprintf("INSERT INTO `orders`(
    `member_sid`, `amount`, `order_date` 
    ) VALUES ( %s, %s, NOW())",
        $_SESSION['user']['id'],
        $total
    );

$mysqli->query($sql);
$order_sid = $mysqli->insert_id; //取得 orders 的 primary key

// 2. 寫入 order_details 表

foreach($keys as $k){
    $d_sql = sprintf("INSERT INTO `order_details`(
      `order_sid`, `product_sid`, `price`, `quantity`
      ) VALUES (%s, %s, %s, %s)",
        $order_sid,
        $k,
        $cart_data[$k]['price'],
        $cart_data[$k]['qty']
        );
    $mysqli->query($d_sql);
}


unset($_SESSION['cart']);







?>
<?php include __DIR__. '/__html_head.php'; ?>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>


    <div class="alert alert-danger" role="alert">
        完成訂購, 感謝您!
    </div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>封面</th>
            <th>書名</th>
            <th>價格</th>
            <th>數量</th>
            <th>小計</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($keys as $k):
            $row = $cart_data[$k];
            ?>
            <tr id="item<?= $row['sid'] ?>" class="data-row" data-sid="<?= $row['sid'] ?>">

                <td><img src="./imgs/small/<?= $row['book_id'] ?>.jpg" alt=""></td>
                <td><?= $row['bookname'] ?></td>
                <td class="price"><?= $row['price'] ?></td>
                <td><?= $row['qty'] ?></td>
                <td class="sub-total"><?= $row['qty']*$row['price']  ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

</div>
<?php include __DIR__. '/__html_foot.php'; ?>

